package com.kmlsolutions.lichesssearch.fetch;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

public class LichessApiSupport {

    public static final String LICHESS_API_URL = "https://lichess.org/api/user/%s/games?nb=100&with_opening=1";

    public static String getGames(String userName) throws IOException /* TODO exception handling */ {
        URL url = new URL(String.format(LICHESS_API_URL, userName));
        URLConnection con = url.openConnection();
//        con.setRequestMethod("GET");
        return getContent(con);
    }

    private static String getContent(URLConnection con){
        StringBuilder sb = new StringBuilder();
        if(con!=null){

            try {

                BufferedReader br =
                        new BufferedReader(
                                new InputStreamReader(con.getInputStream()));

                String input;

                while ((input = br.readLine()) != null){
                    sb.append(input);
                }
                br.close();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

    public static void main(String[] args) throws IOException {
        String foo = LichessApiSupport.getGames("klooth");
        System.out.println("foo=" + foo);
    }
}
