package com.kmlsolutions.lichesssearch.app.controller;

import com.kmlsolutions.lichesssearch.fetch.LichessApiSupport;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

import static org.springframework.util.MimeTypeUtils.APPLICATION_JSON_VALUE;

@RestController
public class SearchController {

    @RequestMapping(value = "/api/lichess/search", produces = APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    public String search(@RequestParam(value="username") String userName) throws IOException {
        return LichessApiSupport.getGames(userName);
    }
}
