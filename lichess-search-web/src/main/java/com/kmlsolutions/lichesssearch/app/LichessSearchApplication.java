package com.kmlsolutions.lichesssearch.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LichessSearchApplication {
    public static void main(String[] args) {
        SpringApplication.run(LichessSearchApplication.class, args);
    }
}
